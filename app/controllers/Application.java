package controllers;


import helper.SlideConverter;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;


public class Application extends Controller {

  public static Result ppt() {
    String pptPath = "/Users/ITPUser/Downloads/Sample.ppt";
    SlideConverter.ppt2png(pptPath);
    return ok(index.render());
  }

  public static Result pptx() {
    String pptxPath = "/Users/ITPUser/Downloads/ControllerBestPractice.pptx";
    SlideConverter.pptx2png(pptxPath);
    return ok(index.render());
  }
  
  public static Result pdf() {
    String pdfPath = "/Users/ITPUser/Downloads/client-side-js-framework-vue.pdf";
    SlideConverter.pdf2png(pdfPath);

    return ok(index.render());
  }
}
