package controllers;

import models.Entry;

import play.*;
import play.mvc.*;
import play.data.Form;

import views.html.*;
import static play.data.Form.form;

public class EntriesController extends Controller {
  public static Result index() {
    return ok(index.render());
  }

  public static Result show(Long id) {
    return ok(show.render(Entry.find.byId(id)));
  }

  public static Result newEntry() {
    return ok(newEntry.render(form(Entry.class)));
  }

  public static Result create() {
    Form<Entry> entryForm = form(Entry.class).bindFromRequest();
    if(entryForm.hasErrors()) {
      return badRequest(newEntry.render(entryForm));
    }
    Entry entry = entryForm.get();
    entry.save();
    return redirect(routes.EntriesController.index());
  }
}
