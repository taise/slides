package helper;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.poi.hslf.model.Slide;
import org.apache.poi.hslf.usermodel.SlideShow;
import org.apache.poi.xslf.usermodel.XMLSlideShow;
import org.apache.poi.xslf.usermodel.XSLFSlide;
import org.apache.pdfbox.PDFBox;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;

public class SlideConverter {
  private final static String outDir
    = "public/images/slides/";

  public static Boolean ppt2png(String pptPath) {
    String fname = filename(pptPath);

    try {
      SlideShow ppt = new SlideShow(new FileInputStream(pptPath));
      Dimension pgsize = ppt.getPageSize();

      Slide[] slide = ppt.getSlides();
      for (int i = 0; i < slide.length; i++) {
        String pptFileName = outFilemamef(fname, i) + ".png";
        System.out.println(pptFileName);

        BufferedImage img
          = new BufferedImage(pgsize.width, pgsize.height, 1);
        Graphics2D graphics
          = rendering(img, pgsize.width, pgsize.height);

        slide[i].draw(graphics);

        FileOutputStream out
          = new FileOutputStream(outDir + pptFileName);
        javax.imageio.ImageIO.write(img, "png", out);
        out.close();
      }
    }catch(FileNotFoundException e){
      System.out.println(e.toString());
      return false;
    }catch(IOException e){
      System.out.println(e.toString());
      return false;
    }
    return true;
  }

  public static Boolean pptx2png(String pptxPath) {
    String fname = filename(pptxPath);

    try {
      XMLSlideShow pptx
        = new XMLSlideShow(new FileInputStream(pptxPath));
      Dimension pgsize = pptx.getPageSize();
      XSLFSlide[] slide = pptx.getSlides();

      for (int i = 0; i < slide.length; i++) {
        String pptFileName = outFilemamef(fname, i) + ".png";
        System.out.println(pptFileName);

        BufferedImage img
          = new BufferedImage(pgsize.width, pgsize.height, BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics
          = rendering(img, pgsize.width, pgsize.height);

        // draw stuff
        slide[i].draw(graphics);

        // save the result 
        FileOutputStream out
          = new FileOutputStream(outDir + pptFileName);
        javax.imageio.ImageIO.write(img, "png", out);
        out.close();
      }
    }catch(FileNotFoundException e){
      System.out.println(e.toString());
      return false;
    }catch(IOException e){
      System.out.println(e.toString());
      return false;
    }
    return true;
  }


  public static Boolean pdf2png(String pdfPath) {
    String fname = filename(pdfPath);

    try{
      PDDocument doc=PDDocument.load(new FileInputStream(pdfPath));
      List<PDPage> pages  = doc.getDocumentCatalog().getAllPages();
      Iterator<PDPage> it = pages.iterator();
      int i = 0;

      while(it.hasNext()){
        String pdfFileName = outFilemamef(fname, i) + ".jpg";
        System.out.println(pdfFileName);

        PDPage page = it.next();
        BufferedImage bi=page.convertToImage();

        ImageIO.write(bi, "jpg", new File(outDir + pdfFileName));
        i++;
      }
      System.out.println("Conversion complete");
    }catch(IOException ie){
      ie.printStackTrace();
      return false;
    }
    return true;
  }


  private static String filename(String filePath){
    File f = new File(filePath);
    return f.getName();
  }

  private static String outFilemamef(String fname, int i) {
    String pageNum = String.format("%04d", (i + 1));
    return fname + "-" + pageNum;
  }

  private static Graphics2D rendering(BufferedImage img, int width, int height) {
    Graphics2D graphics = img.createGraphics();

    // default rendering options
    graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
    graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
    graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BICUBIC);
    graphics.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);

    graphics.setColor(Color.white);
    graphics.clearRect(0, 0, width, height);
    graphics.fill(new Rectangle2D.Float(0, 0, width, height));

    return graphics;
  }
}
