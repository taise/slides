package models;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import play.db.ebean.*;
import play.data.validation.Constraints.*;
import com.avaje.ebean.annotation.CreatedTimestamp;
import com.avaje.ebean.annotation.UpdatedTimestamp;

import static play.db.ebean.Model.Finder;

@Entity
public class Entry extends Model {

  @Id
  public Long id;

  @Required
  public String title;

  public String description;
  
  @CreatedTimestamp
  public Date createdAt;
  @UpdatedTimestamp
  public Date updatedAt;

  public static Finder<Long, Entry> find
    = new Finder<Long, Entry>(Long.class, Entry.class);
}
