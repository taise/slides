package controllers;

import org.junit.*;
import java.util.*;
import play.mvc.*;

import models.Entry;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;


public class EntriesControllerTest {
  @Before
  public void setUp() {
    start(fakeApplication(inMemoryDatabase()));
  }

  @Test
  public void callRoute() {
    Result result = route(fakeRequest(GET, "/"));

    assertThat(status(result)).isEqualTo(OK);
    assertThat(contentType(result)).isEqualTo("text/html");
    assertThat(charset(result)).isEqualTo("utf-8");
  }

  @Test
  public void callNew() {
    Result result = route(fakeRequest(GET, "/entries/new"));

    assertThat(status(result)).isEqualTo(OK);
    assertThat(contentType(result)).isEqualTo("text/html");
    assertThat(charset(result)).isEqualTo("utf-8");
  }

  @Test
  public void callCreateSuccess() {
    Map<String, String> params = new HashMap<String,String>();
    params.put("title", "shibukawa");

    Result result = route(fakeRequest(POST, "/entries")
                .withFormUrlEncodedBody(params));

    assertThat(status(result)).isEqualTo(SEE_OTHER);
    assertThat(redirectLocation(result)).isEqualTo("/");
  }

  @Test
  public void callCreateError() {
    Result result = route(fakeRequest(POST, "/entries"));

    assertThat(status(result)).isEqualTo(BAD_REQUEST);
    assertThat(contentType(result)).isEqualTo("text/html");
    assertThat(charset(result)).isEqualTo("utf-8");
  }

  @Test
  public void callShow() {
    Entry entry = new Entry();
    entry.title = "shibukawa";
    entry.save();

    Result result = route(fakeRequest(GET, "/entries/" + entry.id));

    assertThat(status(result)).isEqualTo(OK);
    assertThat(contentType(result)).isEqualTo("text/html");
    assertThat(charset(result)).isEqualTo("utf-8");
  }
}

