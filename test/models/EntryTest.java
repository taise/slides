package models;

import models.Entry;
import org.junit.*;
import com.avaje.ebean.Ebean;

import static org.fest.assertions.Assertions.*;
import static play.test.Helpers.*;

public class EntryTest {

  @Test
  public void save() {
    Entry entry = new Entry();
    entry.save();

    assertThat(entry.createdAt).isNotNull();
    assertThat(entry.updatedAt).isNotNull();
  }
}
